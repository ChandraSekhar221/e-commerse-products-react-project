import './App.css';
import Header from './Components/Header';

import Home from './Components/Home/Home';

import Products from './Components/Products/Products';

import AllProducts from './Components/AllProducts/AllProducts';

import { Switch, Route } from 'react-router-dom'

import CreateNewProduct from './Components/CreateNewProduct/CreateNewProduct';

import React, { Component } from 'react'

import axios from 'axios'

import validator from 'validator';

import DeleteProduct from './Components/DeleteProducts/DeleteProduct';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoaded: false,
      isError: false,
      data: [],
      originalData: [],
      userImageUrl: '',
      inputTitle: '',
      inputDescription: '',
      inputCategory: '',
      inputRating: '',
      inputPrice: '',
      productDetails: null,
      deleteProductDetails: null,
      userRatingValidate: true,
      userPriceValidate: true,
      newProductDataValidate: false,
      newProductDetails: {
        title: '',
        category: '',
        description: '',
        image: '',
        price: '',
        rating: { rate: '' },
        newProductUrlValidate: true,
        newProductPriceValidate: true,
        newProductRatingValidate: true,
      }
    }


    this.handleProductclick = this.handleProductclick.bind(this)
    this.handleDeleteProductclick = this.handleDeleteProductclick.bind(this)
    this.handleUserEnteredTitle = this.handleUserEnteredTitle.bind(this)
    this.handleUserEneteredDescription = this.handleUserEneteredDescription.bind(this)
    this.handleUserEneteredCategory = this.handleUserEneteredCategory.bind(this)
    this.handleUserEneteredRating = this.handleUserEneteredRating.bind(this)
    this.handleUserEneteredPrice = this.handleUserEneteredPrice.bind(this)
    this.handleCreateProduct = this.handleCreateProduct.bind(this)
    this.handleUpdateProduct = this.handleUpdateProduct.bind(this)
    this.handleDeleteProduct = this.handleDeleteProduct.bind(this)
    this.handleUserEnteredNewProductTitle = this.handleUserEnteredNewProductTitle.bind(this)
    this.handleUserEnteredNewProductCategoty = this.handleUserEnteredNewProductCategoty.bind(this)
    this.handleUserEnteredNewProductDescription = this.handleUserEnteredNewProductDescription.bind(this)
    this.handleUserEnteredNewProductImageUrl = this.handleUserEnteredNewProductImageUrl.bind(this)
    this.handleUserEnteredNewProductPrice = this.handleUserEnteredNewProductPrice.bind(this)
    this.handleNewProductFormSubmit = this.handleNewProductFormSubmit.bind(this)
    this.handleUserEnteredNewProductRating = this.handleUserEnteredNewProductRating.bind(this)
  }

  // Update Product Event Handlers

  handleProductclick(id) {
    let productDetails = this.state.data.filter((eachProduct) => {
      if (id === eachProduct.id) {
        return eachProduct
      } return false
    }).pop()
    this.setState({
      productDetails: productDetails,
      inputTitle: '',
      inputDescription: '',
      inputCategory: '',
      inputRating: '',
      inputPrice: '',
    })
  }
  handleDeleteProductclick(id) {
    let productDetails = this.state.data.filter((eachProduct) => {
      if (id === eachProduct.id) {
        return eachProduct
      } return false
    }).pop()
    this.setState({
      deleteProductDetails: productDetails,
      inputTitle: '',
      inputDescription: '',
      inputCategory: '',
      inputRating: '',
      inputPrice: '',
    })
  }
  handleUserEnteredTitle(e, id) {
    if (e.target.value.trim()) {
      this.setState({
        productDetails: { ...this.state.productDetails, title: e.target.value },
        inputTitle: e.target.value
      })
    } else {
      let originalProductTitle = this.state.originalData.filter((originalProduct) => {
        if (id === originalProduct.id) {
          return originalProduct
        } else {
          return null
        }
      }).pop().title
      this.setState({
        productDetails: { ...this.state.productDetails, title: originalProductTitle },
        inputTitle: '',
      })
    }
  }
  handleUserEneteredDescription(e, id) {
    if (e.target.value.trim()) {
      this.setState({
        productDetails: { ...this.state.productDetails, description: e.target.value },
        inputDescription: e.target.value
      })
    } else {
      let originalProductDescription = this.state.originalData.filter((originalProduct) => {
        if (id === originalProduct.id) {
          return originalProduct
        } else {
          return null
        }
      }).pop().description
      this.setState({
        productDetails: { ...this.state.productDetails, description: originalProductDescription },
        inputDescription: '',
      })
    }
  }
  handleUserEneteredCategory(e, id) {
    if (e.target.value.trim()) {
      this.setState({
        productDetails: { ...this.state.productDetails, category: e.target.value },
        inputCategory: e.target.value
      })
    } else {
      let originalProductCategory = this.state.originalData.filter((originalProduct) => {
        if (id === originalProduct.id) {
          return originalProduct
        } else {
          return null
        }
      }).pop().category
      this.setState({
        productDetails: { ...this.state.productDetails, category: originalProductCategory },
        inputCategory: '',
      })
    }

  }
  handleUserEneteredRating(e, id) {
    if (e.target.value.trim()) {
      if (validator.isNumeric(e.target.value.trim())) {
        this.setState({
          productDetails: {
            ...this.state.productDetails,
            rating: {
              rate: e.target.value,
              count: this.state.productDetails.rating.count
            }
          },
          userRatingValidate: true,
          inputRating: e.target.value,
        })
      } else {
        this.setState({
          userRatingValidate: false,
          inputRating: e.target.value,
        })
      }
    }
    else {
      let originalProductRating = this.state.originalData.filter((originalProduct) => {
        if (id === originalProduct.id) {
          return originalProduct
        } else {
          return null
        }
      }).pop().rating.rate
      this.setState({
        productDetails: {
          ...this.state.productDetails,
          rating: {
            rate: originalProductRating,
            count: this.state.productDetails.rating.count
          }
        },
        inputRating: '',
        userRatingValidate: true,
      })
    }
  }
  handleUserEneteredPrice(e, id) {
    if (e.target.value.trim()) {
      if (validator.isNumeric(e.target.value.trim())) {
        this.setState({
          productDetails: {
            ...this.state.productDetails,
            price: e.target.value,
          },
          userPriceValidate: true,
          inputPrice: e.target.value,
        })
      } else {
        this.setState({
          userPriceValidate: false,
          inputPrice: e.target.value,
        })
      }
    }
    else {
      let originalProductPrice = this.state.originalData.filter((originalProduct) => {
        if (id === originalProduct.id) {
          return originalProduct
        } else {
          return null
        }
      }).pop().price
      this.setState({
        productDetails: {
          ...this.state.productDetails,
          price: originalProductPrice,
        },
        inputPrice: '',
        userPriceValidate: true,
      })
    }
  }

  // Create product Event Hanlers

  handleUserEnteredNewProductTitle(e) {
    this.setState({
      newProductDetails: { ...this.state.newProductDetails, title: e.target.value, }
    })
  }

  handleUserEnteredNewProductCategoty(e) {
    this.setState({
      newProductDetails: { ...this.state.newProductDetails, category: e.target.value, },
    })
  }

  handleUserEnteredNewProductDescription(e) {
    this.setState({
      newProductDetails: { ...this.state.newProductDetails, description: e.target.value, },
    })
  }

  handleUserEnteredNewProductImageUrl(e) {
    if (e.target.value.trim()) {
      if (validator.isURL(e.target.value.trim())) {
        this.setState({
          newProductDetails: {
            ...this.state.newProductDetails, image: e.target.value.trim(),
            newProductUrlValidate: true,
          },
        })
      } else {
        this.setState({
          newProductDetails: {
            ...this.state.newProductDetails, image: e.target.value.trim(),
            newProductUrlValidate: false,
          },
        })
      }
    } else {
      this.setState({
        newProductDetails: {
          ...this.state.newProductDetails, image: '',
          newProductUrlValidate: true,
        },
      })
    }
  }

  handleUserEnteredNewProductPrice(e) {
    if (e.target.value.trim()) {
      if (validator.isNumeric(e.target.value.trim())) {
        this.setState({
          newProductDetails: {
            ...this.state.newProductDetails, price: e.target.value.trim(),
            newProductPriceValidate: true,
          },
        })
      } else {
        this.setState({
          newProductDetails: {
            ...this.state.newProductDetails, price: e.target.value.trim(),
            newProductPriceValidate: false,
          },
        })
      }
    } else {
      this.setState({
        newProductDetails: {
          ...this.state.newProductDetails, price: '',
          newProductPriceValidate: true,
        },
      })
    }
  }

  handleUserEnteredNewProductRating(e) {
    if (e.target.value.trim()) {
      if (validator.isNumeric(e.target.value.trim())) {
        this.setState({
          newProductDetails: {
            ...this.state.newProductDetails, rating: { rate: e.target.value.trim() },
            newProductRatingValidate: true,
          },
        })
      } else {
        this.setState({
          newProductDetails: {
            ...this.state.newProductDetails, rating: { rate: e.target.value.trim() },
            newProductRatingValidate: false,
          },
        })
      }
    } else {
      this.setState({
        newProductDetails: {
          ...this.state.newProductDetails, rating: { rate: '' },
          newProductRatingValidate: true,
        },
      })
    }
  }

  handleNewProductFormSubmit(e) {
    e.preventDefault()
    console.log('Onclick form data', this.state.newProductDetails)
    this.setState({
      newProductDetails: {
        ...this.state.newProductDetails,
        title: this.state.newProductDetails.title.trim().length > 0 ? this.state.newProductDetails.title.trim() : false,
        category: this.state.newProductDetails.category.trim().length > 0 ? this.state.newProductDetails.category.trim() : false,
        description: this.state.newProductDetails.description.trim().length > 0 ? this.state.newProductDetails.description.trim() : false,
        image: this.state.newProductDetails.image.trim().length > 0 ? this.state.newProductDetails.image.trim() : false,
        price: this.state.newProductDetails.price.trim().length > 0 ? this.state.newProductDetails.price.trim() : false,
        rating: this.state.newProductDetails.rating.rate.length > 0 ? this.state.newProductDetails.rating : { rate: false },
      }

    }, () => {
      console.log('After Minor data validate', this.state.newProductDetails)
      if (this.state.newProductDetails.title &&
        this.state.newProductDetails.category &&
        this.state.newProductDetails.description &&
        this.state.newProductDetails.image &&
        this.state.newProductDetails.price &&
        this.state.newProductDetails.rating.rate &&
        this.state.newProductDetails.newProductUrlValidate &&
        this.state.newProductDetails.newProductPriceValidate &&
        this.state.newProductDetails.newProductRatingValidate
      ) {
        this.setState({
          newProductDataValidate: true,
        }, () => { console.log('After successfulldata validation', this.state.newProductDetails) })

      }
    })
  }

  // CRUD API Calls 

  handleCreateProduct(newProductDetails) {
    axios.post('https://fakestoreapi.com/products',
      newProductDetails
    ).then(respose => {
      return respose.data
    }).then((data) => {
      newProductDetails['id'] = this.state.data.length + 1
      this.setState({
        data: [...this.state.data, newProductDetails],
        newProductDataValidate: false,
        newProductDetails: {
          title: '',
          category: '',
          description: '',
          image: '',
          price: '',
          rating: { rate: '' },
          newProductUrlValidate: true,
          newProductPriceValidate: true,
          newProductRatingValidate: true,
        },
      })

    })
      .catch(err => {
        console.log(err)
      })

  }

  handleUpdateProduct(updateProductDetails) {
    console.log(updateProductDetails)
    axios.put(`https://fakestoreapi.com/products/${updateProductDetails.id}`,
      updateProductDetails
    ).then(response => {
      console.log(response)
      let updatedData = this.state.data.map(product => {
        if (updateProductDetails.id === product.id) {
          product.title = updateProductDetails.title
          product.description = updateProductDetails.description
          product.price = updateProductDetails.price
          product.category = updateProductDetails.category
          product.rating.rate = updateProductDetails.rating.rate

          return product
        } else {
          return product
        }
      })
      this.setState({
        inputTitle: '',
        inputDescription: '',
        inputCategory: '',
        inputRating: '',
        inputPrice: '',
        data: updatedData,
        originalData: updatedData,
      })
    })

  }

  handleDeleteProduct(id) {
    console.log(id)
    console.log(this.state.data)
    axios.delete(`https://fakestoreapi.com/products/${id}`)
      .then(response => {
        console.log(response)
        return response.data
      }).then((product) => {
        let newData = this.state.data.filter((eachProduct) => eachProduct.id !== id)
        console.log(newData)
        this.setState({
          data: newData,
          deleteProductDetails: null,
        })
      })
  }

  componentDidMount() {
    let url = 'https://fakestoreapi.com/products'
    axios.get(url)
      .then(response => {
        if (response.status !== 200) {
          throw Error('Failed to fetch')
        }
        return response.data
      })
      .then(products => {
        this.setState({
          isLoaded: true,
          data: products,
          originalData: JSON.parse(JSON.stringify(products)),
        })
      })
      .catch((err) => {
        this.setState({
          isLoaded: true,
          isError: err.message,
        })
      })
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route exact path='/' component={Home} />

          <Route each path='/products' render={(props) => (<AllProducts

            isLoaded={this.state.isLoaded}
            isError={this.state.isError}
            products={this.state.data}

          />)} />

          <Route exact path='/updateProducts' render={(props) => (<Products {...props}
            parentState={this.state}
            handleProductclick={this.handleProductclick}
            handleUserEnteredTitle={this.handleUserEnteredTitle}
            handleUserEneteredDescription={this.handleUserEneteredDescription}
            handleUserEneteredCategory={this.handleUserEneteredCategory}
            handleUserEneteredRating={this.handleUserEneteredRating}
            handleUserEneteredPrice={this.handleUserEneteredPrice}
            handleUpdateProduct={this.handleUpdateProduct}
          />)} />
          <Route exact path='/createNewProduct' render={() => (<CreateNewProduct
            newProductDetails={this.state.newProductDetails}
            newProductDataValidate={this.state.newProductDataValidate}
            handleUserEnteredNewProductTitle={this.handleUserEnteredNewProductTitle}
            handleUserEnteredNewProductPrice={this.handleUserEnteredNewProductPrice}
            handleUserEnteredNewProductCategoty={this.handleUserEnteredNewProductCategoty}
            handleUserEnteredNewProductDescription={this.handleUserEnteredNewProductDescription}
            handleUserEnteredNewProductImageUrl={this.handleUserEnteredNewProductImageUrl}
            handleUserEnteredNewProductRating={this.handleUserEnteredNewProductRating}
            handleNewProductFormSubmit={this.handleNewProductFormSubmit}
            handleCreateProduct={this.handleCreateProduct}
          />)} />
          <Route exact path='/deleteProduct' render={() => (<DeleteProduct
            parentState={this.state}
            handleDeleteProductclick={this.handleDeleteProductclick}
            deleteProductDetails={this.state.deleteProductDetails}
            handleDeleteProduct={this.handleDeleteProduct}
          />)} />
        </Switch>
      </div>
    );
  }
}
export default App

