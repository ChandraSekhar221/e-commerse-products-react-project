import React, { Component } from 'react'

import Loader from '../Loader/Loader'

import Fetchfail from '../Fetchfail/Fetchfail'

import './AllProducts.css'


class AllProducts extends Component {
    render() {
        const { products, isLoaded, isError } = this.props
        console.log(isLoaded , isError , products)
        console.log(this.props)
        if (!isLoaded) {
            return (
                <div className="d-flex flex-column justify-content-center align-items-center m-auto pt-5 spinner-container-bg">
                    <Loader />
                </div>
            )
        } else {
            if (isError) {
                return (
                    <Fetchfail Error={isError} />
                )
            } else {
                if (products.length) {
                    return (
                        <div className="d-flex flex-column justify-content-center align-items-center" style={{backgroundColor:'#6b599e'}}>
                            <h1 className="m-5 pt-5 product-heder">Welcome to Our Products</h1>
                            <div className="d-flex flex-wrap mx-1 my-0  align-self-center">
                                {products.map(each => {
                                    return (
                                        <div key={each.id} className='d-flex flex-column product-container m-3 shadow p-3 mb-5 bg-white rounded'>
                                            <h3 className='product-name'>{each.title}</h3>
                                            <img src={each.image} alt="" className='product-image mb-2' />
                                            <p className="pt-2">Category : <span className="product-price">{each.category}</span></p>
                                            <p className="pt-2">Description : <span className="text-secondary">{each.description}</span></p>
                                            <p className="pt-2">Price : <span className="product-price">{each.price}</span></p>
                                            <p className="pt-2">Rating : <span className="product-price">{each.rating.rate}</span></p>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    )
                }
                else {
                    return (
                        <div className="d-flex justify-content-center align-items-center m-5 p-5">
                            <p>Oops! No products found now. We will update the products soon. Try after some time.</p>
                        </div>
                    )
                }
            }
        }
    }
}

export default AllProducts
