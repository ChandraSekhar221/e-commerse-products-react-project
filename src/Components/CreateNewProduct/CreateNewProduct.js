import React, { Component } from 'react'

// import validator from 'validator';

import CreateNewProductView from './CreateNewProductView';

class CreateNewProduct extends Component {

    // constructor(props) {
    //     super(props)

    //     this.state = {
    //         title: '',
    //         category: '',
    //         description: '',
    //         image: '',
    //         price: '',
    //         newProductUrlValidate: true,
    //         newProductPriceValidate: true,
    //         newProductDataValidate: false,
    //     }
    //     this.handleFormSubmit = this.handleFormSubmit.bind(this)
    //     this.handleUserEnteredTitle = this.handleUserEnteredTitle.bind(this)
    //     this.handleUserEnteredCategoty = this.handleUserEnteredCategoty.bind(this)
    //     this.handleUserEnteredDescription = this.handleUserEnteredDescription.bind(this)
    //     this.handleUserEnteredImageUrl = this.handleUserEnteredImageUrl.bind(this)
    //     this.handleUserEnteredPrice = this.handleUserEnteredPrice.bind(this)
    // }

    // handleUserEnteredTitle(e) {
    //     if (e.target.value.trim()) {
    //         this.setState({
    //             title: e.target.value.trim(),
    //         })
    //     }
    // }

    // handleUserEnteredCategoty(e) {
    //     if (e.target.value.trim()) {
    //         this.setState({
    //             category: e.target.value.trim(),
    //         })
    //     }
    // }

    // handleUserEnteredDescription(e) {
    //     if (e.target.value.trim()) {
    //         this.setState({
    //             description: e.target.value.trim(),
    //         })
    //     }

    // }

    // handleUserEnteredImageUrl(e) {
    //     if (e.target.value.trim()) {
    //         if (validator.isURL(e.target.value.trim())) {
    //             this.setState({
    //                 image: e.target.value.trim(),
    //                 newProductUrlValidate: true,
    //             })
    //         } else {
    //             this.setState({
    //                 image: e.target.value.trim(),
    //                 newProductUrlValidate: false,
    //             })
    //         }
    //     } else {
    //         this.setState({
    //             image: '',
    //             newProductUrlValidate: true,
    //         })
    //     }
    // }

    // handleUserEnteredPrice(e) {
    //     if (e.target.value.trim()) {
    //         if (validator.isNumeric(e.target.value.trim())) {
    //             this.setState({
    //                 price: e.target.value.trim(),
    //                 newProductPriceValidate: true,
    //             })
    //         } else {
    //             this.setState({
    //                 price: e.target.value.trim(),
    //                 newProductPriceValidate: false,
    //             })
    //         }
    //     } else {
    //         this.setState({
    //             price: '',
    //             newProductPriceValidate: true,
    //         })
    //     }
    // }

    // handleFormSubmit(e) {
    //     e.preventDefault()
    //     console.log('Onclick form data', this.state)
    //     this.setState({
    //         title: this.state.title.length > 0 ? this.state.title : false,
    //         category: this.state.category.length > 0 ? this.state.category : false,
    //         description: this.state.description.length > 0 ? this.state.description : false,
    //         image: this.state.image.length > 0 ? this.state.image : false,
    //         price: this.state.price.length > 0 ? this.state.price : false,
    //     }, () => {
    //         console.log('After Minor data validate', this.state)
    //         if (this.state.title &&
    //             this.state.category &&
    //             this.state.description &&
    //             this.state.image &&
    //             this.state.price &&
    //             this.state.newProductUrlValidate &&
    //             this.state.newProductPriceValidate
    //         ) {
    //             this.setState({
    //                 newProductDataValidate: true
    //             }, () => { console.log('After successfulldata validation', this.state) })

    //         }
    //     })
    // }

    render() {
        let newProductDetails = this.props.newProductDetails
        let newProductDataValidate = this.props.newProductDataValidate
        let handleUserEnteredNewProductTitle = this.props.handleUserEnteredNewProductTitle
        let handleUserEnteredNewProductPrice = this.props.handleUserEnteredNewProductPrice
        let handleUserEnteredNewProductRating = this.props.handleUserEnteredNewProductRating
        let handleUserEnteredNewProductCategoty = this.props.handleUserEnteredNewProductCategoty
        let handleUserEnteredNewProductDescription = this.props.handleUserEnteredNewProductDescription
        let handleUserEnteredNewProductImageUrl = this.props.handleUserEnteredNewProductImageUrl
        let handleNewProductFormSubmit = this.props.handleNewProductFormSubmit
        let handleCreateProduct = this.props.handleCreateProduct
        return (
            <div className='container-fluid' style={{backgroundColor:'#3c35f2',minHeight:'100vh', color: '#212101'}}>
                <div className='row'>
                    <div className='col-10 col-md-5 p-4 mx-auto row row-eq-height' style={{backgroundColor:'#e8c564', marginTop:'7rem',marginBottom:'5rem',borderRadius:'2rem'}}>
                        <h2 className='m-0'>Enter The new product Details here</h2>
                        <form className=''>
                            <div className="form-group mb-3">
                                <label htmlFor="title">Title : </label>
                                <input type="text" className="form-control" id="title" value={!newProductDetails.title ? '' : newProductDetails.title} placeholder="Enter Title of the product"
                                    onChange={handleUserEnteredNewProductTitle}
                                />
                                {newProductDetails.title === false ? <small className='text-danger' >*Enter Title</small> : null}
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="category">Category : </label>
                                <input type="text" className="form-control" id="category" value={!newProductDetails.category ? '' : newProductDetails.category} placeholder="Enter Category of the product"
                                    onChange={handleUserEnteredNewProductCategoty}
                                />
                                {newProductDetails.category === false ? <small className='text-danger'>*Enter Category</small> : null}
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="description">Description : </label>
                                <input type="text" className="form-control" id="description" value={!newProductDetails.description ? '' : newProductDetails.description} placeholder="Enter Description of the product"
                                    onChange={handleUserEnteredNewProductDescription}
                                />
                                {newProductDetails.description === false ? <small className='text-danger'>*Enter Description</small> : null}
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="image-url">Image URL : </label>
                                <input type="text" className="form-control" id="image-url" value={!newProductDetails.image ? '' : newProductDetails.image } placeholder="Enter the url of the Image of the product"
                                    onChange={handleUserEnteredNewProductImageUrl}
                                />
                                {newProductDetails.image === false ? <small className='text-danger'>*Enter Image URL</small> : null}
                                {newProductDetails.newProductUrlValidate === false ? <small className='text-danger'>*Enter valid Image URL</small> : null}

                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="price">Price : </label>
                                <input type="text" className="form-control" id="price" value={!newProductDetails.price ? '' :newProductDetails.price } placeholder="Enter price of the product"
                                    onChange={handleUserEnteredNewProductPrice}
                                />
                                {newProductDetails.price === false ? <small className='text-danger'>*Enter Price</small> : null}
                                {newProductDetails.newProductPriceValidate === false ? <small className='text-danger'>*Enter valid Price. Price should only consists of digits</small> : null}
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="rating">Rating : </label>
                                <input type="text" className="form-control" id="rating" value={!newProductDetails.rating.rate ? '' : newProductDetails.rating.rate} placeholder="Enter price of the product"
                                    onChange={handleUserEnteredNewProductRating}
                                />
                                {newProductDetails.rating.rate === false ? <small className='text-danger'>*Enter Rating</small> : null}
                                {newProductDetails.newProductRatingValidate === false ? <small className='text-danger'>*Enter valid Rating. Price should only consists of digits</small> : null}
                            </div>
                            <button type="submit" onClick={handleNewProductFormSubmit} className="btn btn-danger my-3">Submit here to show Preview</button>
                        </form>
                    </div>
                    <div className='col-10 col-md-5 p-4 mx-auto row row-eq-height' style={{backgroundColor:'#e8c564', marginTop:'7rem',marginBottom:'5rem',borderRadius:'2rem'}}>
                        {newProductDataValidate ? <CreateNewProductView
                            productDetails={newProductDetails}
                            handleCreateProduct={handleCreateProduct}
                        /> : <div className='d-flex justify-content-center align-items-center text-center text-white'>
                            <h1>Fill all fields and click the submit button to see the preview of the product</h1>
                        </div>}
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateNewProduct

