import React, { Component } from 'react'

import './CreateNewProduct.css'

class CreateNewProductView extends Component {
    render() {
        let { productDetails } = this.props
        let {handleCreateProduct} = this.props
        console.log(handleCreateProduct)
        console.log(productDetails)
        return (
            <div className=''>
                <img className='image w-100' style={{borderRadius:'1rem'}} src={productDetails.image} alt='' />
                <h3 className='mt-3'>Title : {productDetails.title}</h3>
                <h5 className='mt-3'>Category : {productDetails.category}</h5>
                <p className='mt-3'>Description : {productDetails.description}</p>
                <p className='mt-3'>Price : {productDetails.price}</p>
                <p className='mt-3'>Rating : {productDetails.rating.rate}</p>
                <button className='btn btn-danger'
                onClick={() => {handleCreateProduct({
                    title : productDetails.title ,
                    image : productDetails.image ,
                    category : productDetails.category ,
                    description : productDetails.description ,
                    price : productDetails.price ,
                    rating : productDetails.rating,
                })}}
                >Click here to Post the new Product</button>
            </div>
        )
    }
}

export default CreateNewProductView
