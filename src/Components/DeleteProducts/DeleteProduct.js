import React, { Component } from 'react'
import Fetchfail from '../Fetchfail/Fetchfail'
import Loader from '../Loader/Loader'
import NoProducts from '../Noproducts/Noproducts'
import DeleteProductList from './DeleteProductList'
import DeleteSelectedProduct from './DeleteSelectedProduct'


class DeleteProduct extends Component {
    render() {
        console.log(this.props)
        let { isLoaded, isError, data, } = this.props.parentState
        let handleDeleteProduct = this.props.handleDeleteProduct
        console.log(handleDeleteProduct)
        if (!isLoaded) {
            return <Loader />
        } else if (isError) {
            return <Fetchfail Error={isError} />
        } else {
            if (!data.length) {
                return <NoProducts />
            } else {
                return (
                    <div className="container-fluid" style={{ minHeight: '93vh',backgroundColor:'#e0c3fa' }}>
                        <div className="row">
                            <div className="col-6 col-md-6 col-lg-5 col-xl-4 col-xxl-3 products-container">
                                {data.map(product => {
                                    return <DeleteProductList
                                        key={product.title}
                                        handleDeleteProductclick={() => this.props.handleDeleteProductclick(product.id)}
                                        product={product}
                                    />
                                })}
                            </div>
                            <div className='col-6 col-md-6 col-lg-7 col-xl-8 col-xxl-9 products-container'>
                                <DeleteSelectedProduct productDetails={this.props.deleteProductDetails} handleDeleteProduct ={handleDeleteProduct} />
                            </div>
                        </div>
                    </div>
                )
            }
        }
    }
}

export default DeleteProduct
