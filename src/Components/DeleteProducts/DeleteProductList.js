import React, { Component } from 'react'

class DeleteProductList extends Component {
    render() {
        console.log(this.props)
        const { id, title, image, price, description, category, rating } = this.props.product
        return (
            <div className="card mb-3" onClick={() => this.props.handleDeleteProductclick(id)}>
                <img src={image} className="card-img-top" alt="" />
                <div className="card-body">
                    <h5 className="card-title">{title}</h5>
                    <p className="card-text">Category : {category}</p>
                    <p className="card-text">Description : {description.slice(0, 15)}....</p>
                    <p className="card-text">Rating : {rating.rate}</p>
                    <p className="card-text">Unit Price : ${price}</p>
                </div>
            </div>
        )
    }
}

export default DeleteProductList
