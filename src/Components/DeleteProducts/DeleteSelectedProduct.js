import React, { Component } from 'react'

class DeleteSelectedProduct extends Component {
    render() {
        const { productDetails } = this.props
        console.log(productDetails)
        let handleDeleteProduct = this.props.handleDeleteProduct
        console.log(handleDeleteProduct)
        if (!productDetails) {
            return <p className='text-center p-5 h1 text-info'>Click on the product to be Deleted</p>
        }
        return (
            <div className="card" style={{minHeight : '92vh'}}>
                <img src={productDetails.image} className="card-img-top w-25 m-2" alt="" />
                <div className="card-body">
                    <h4 className="card-title">Title : {productDetails.title}</h4>
                    <p className="card-text" style={{marginBottom : '8px'}} >Description : {productDetails.description} </p>
                    <p className="card-text" style={{marginBottom : '8px'}} >Category : {productDetails.category} </p>
                    <p className="card-text" style={{marginBottom : '8px'}} >Unit Price : ${productDetails.price}</p>
                    <div className='d-flex flex-column justify-content-center'>
                        <button className='btn btn-primary' onClick={() =>this.props.handleDeleteProduct(productDetails.id)}>Clickhere to confirm Deletion</button>
                    </div>
                    <p className='mt-3 text-danger h3'>*Click on the other element whichever you want to delete</p>
                </div>
            </div>
        )
    }
}

export default DeleteSelectedProduct
