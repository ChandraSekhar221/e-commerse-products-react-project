import React from 'react'

import './Fetchfail.css'

function Fetchfail({ Error }) {
    return (
        <div className='d-flex flex-column justify-content-center align-items-center fetchFail text-center '>
            <h1 className='errTitle font-weight-bold mx-5'>{Error}</h1>
            <p className='errMsg mx-5 h3 text-center'>We are having issues currently. Try after some time</p>

        </div>
    )
}

export default Fetchfail
