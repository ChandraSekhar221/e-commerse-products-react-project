import React, { Component } from 'react'

import { Link } from 'react-router-dom'

class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <div className="container-fluid">
                    <p className='text-white mr-auto my-auto'>E-Commerce</p>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="justify-content-center collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link to='/' className="nav-link active">Home</Link>
                            </li><li className="nav-item">
                                <Link to='/products' className="nav-link active">Products</Link>
                            </li>
                            <li className="nav-item">
                                <Link to='/updateProducts' className="nav-link active">Update Produts</Link>
                            </li>
                            <li className="nav-item">
                                <Link to='/createNewProduct' className="nav-link active">Create New Product</Link>
                            </li>
                            <li className="nav-item">
                                <Link to='/deleteProduct' className="nav-link active">Delete Product</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Header
