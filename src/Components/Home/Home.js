import React from 'react'

import './Home.css'

export default function Home() {
    return (
        <div className='cointainer-fluid'>
            <div className='row m-0'>
                <div className='col-12 mx-0 px-5 home-container'>
                    <h1>Welcome to our E-Commerce website. We are exploring a lot products</h1>
                </div>
            </div>
        </div>
    )
}
