import React from 'react'

import './Loader.css'

export default function Loader() {
    return (
        <div className='loader-container d-flex justify-content-center'>
            <p className='h1'>Loading.... {' '}</p>
            <span className="loader"></span>
        </div>
    )
}
