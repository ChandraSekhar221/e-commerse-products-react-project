import React from 'react'

import './Noproducts.css'

function NoProducts() {
    return (
        <div className='d-flex flex-column justify-content-center align-items-center fetchFail text-center m-auto'>
            <h1 className='errTitle font-weight-bold mx-5'>We have no products right now.</h1>
            <h3 className='errTitle font-weight-bold mx-5'>We are updating the products, So please try after some time.</h3>
        </div>
    )
}

export default NoProducts