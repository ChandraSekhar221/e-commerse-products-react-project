import React, { Component } from 'react'
import Fetchfail from '../Fetchfail/Fetchfail'
import Loader from '../Loader/Loader'
import NoProducts from '../Noproducts/Noproducts'
import EachProduct from '../Product/EachProduct'
import ProductDetails from '../productDetails/ProductDetails'

import './Products.css'

class Products extends Component {


    render() {
        console.log(this.props)
        let { isLoaded, isError, data, productDetails} = this.props.parentState
        let { userRatingValidate, userPriceValidate } = this.props.parentState
        let { inputTitle, inputDescription, inputCategory, inputRating, inputPrice } = this.props.parentState
        if (!isLoaded) {
            return <Loader />
        } else if (isError) {
            return <Fetchfail Error={isError} />
        } else {
            if (!data.length) {
                return <NoProducts />
            } else {
                return (
                    <div className="container-fluid" style={{ minHeight: '93vh',backgroundColor:'#e0c3fa' }}>
                        <div className="row">
                            <div className="col-6 col-md-6 col-lg-5 col-xl-4 products-container mx-0">
                                {data.map(product => {
                                    return <EachProduct
                                        key={product.title}
                                        handleProductclick={() => this.props.handleProductclick(product.id)}
                                        product={product}
                                    />
                                })}
                            </div>
                            <div className='col-6 col-md-6 col-lg-7 col-xl-8 products-container mx-0'>
                                <ProductDetails
                                    productDetails={productDetails}
                                    handleUserEnteredTitle={this.props.handleUserEnteredTitle}
                                    handleUserEneteredDescription={this.props.handleUserEneteredDescription}
                                    handleUserEneteredCategory={this.props.handleUserEneteredCategory}
                                    handleUserEneteredRating={this.props.handleUserEneteredRating}
                                    handleUserEneteredPrice={this.props.handleUserEneteredPrice}
                                    ratingValidate={userRatingValidate}
                                    priceValidate={userPriceValidate}
                                    inputTitle={inputTitle}
                                    inputDescription={inputDescription}
                                    inputCategory={inputCategory}
                                    inputRating={inputRating}
                                    inputPrice={inputPrice}
                                    handleUpdateProduct={this.props.handleUpdateProduct}
                                />
                            </div>
                        </div>
                    </div>
                )
            }
        }
    }
}

export default Products
