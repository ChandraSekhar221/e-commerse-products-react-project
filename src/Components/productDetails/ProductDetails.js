import React, { Component } from 'react'

class ProductDetails extends Component {
    render() {
        const { productDetails } = this.props
        if (!productDetails) {
            return (
                <div style={{minHeight:'93vh'}}>
                    <p className='text-center p-5 h1 text-info'>Click on the product to update Details...</p>
                </div>
            )

        }
        return (
            <div className="card" style={{ minHeight: '93vh' , borderRadius:'1rem'}}>
                <img src={productDetails.image} className="card-img-top w-25 m-2" alt="" />
                <div className="card-body">
                    <h4 className="card-title">Title : {productDetails.title}</h4>
                    <p className="card-text" style={{ marginBottom: '8px' }} >Description : {productDetails.description} </p>
                    <p className="card-text" style={{ marginBottom: '8px' }} >Category : {productDetails.category} </p>
                    <p className="card-text" style={{ marginBottom: '8px' }} >Rating : {productDetails.rating.rate} </p>
                    <p className="card-text" style={{ marginBottom: '8px' }} >Unit Price : ${productDetails.price}</p>
                    <div className='d-flex flex-column justify-content-center'>
                        <div className='m-2'>
                            <label htmlFor='userSelectedTitle' className='px-2'>New Title :</label>
                            <input type='text' id='userSelectedTitle' value={this.props.inputTitle} placeholder='Enter new Title'
                                onChange={(e) => this.props.handleUserEnteredTitle(e, productDetails.id)}
                            ></input>
                        </div>
                        <div className='m-2'>
                            <label htmlFor='userSelectedDescription' className='px-2'>New Description :</label>
                            <input type='text' id='userSelectedDescription' value={this.props.inputDescription} placeholder='Enter new Description'
                                onChange={(e) => this.props.handleUserEneteredDescription(e, productDetails.id)}
                            ></input>
                        </div>
                        <div className='m-2'>
                            <label htmlFor='userSelectedCategory' className='px-2'>New Category :</label>
                            <input type='text' id='userSelectedCategory' value={this.props.inputCategory} placeholder='Enter new Category'
                                onChange={(e) => this.props.handleUserEneteredCategory(e, productDetails.id)}
                            ></input>
                        </div>
                        <div className='m-2'>
                            <label htmlFor='userSelectedPrice' className='px-2'>New Price :</label>
                            <input type='text' id='userSelectedPrice' value={this.props.inputPrice} placeholder='Enter new price'
                                onChange={(e) => this.props.handleUserEneteredPrice(e, productDetails.id)}
                            ></input>
                            {this.props.priceValidate ? null : <p className='text-danger'>*Price Sholud only consists of digits.</p>}
                        </div>
                        <div className='m-2'>
                            <label htmlFor='userSelectedRating' className='px-2'>New Rating :</label>
                            <input type='text' id='userSelectedRating' value={this.props.inputRating} placeholder='Enter new Rating'
                                onChange={(e) => this.props.handleUserEneteredRating(e, productDetails.id)}
                            ></input>
                            {this.props.ratingValidate ? null : <p className='text-danger'>*Rating Sholud only consists of digits.</p>}
                        </div>
                        <button className='btn btn-primary' onClick={() => this.props.handleUpdateProduct(productDetails)}>Clickhere to save updated product details</button>
                        <p className='mt-3 text-danger h3'>*Click on the other element whichever you want to Update</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductDetails





